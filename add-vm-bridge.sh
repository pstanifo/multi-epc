#!/bin/bash

cat <<EOF | sudo tee /etc/systemd/network/br0.netdev > /dev/null
[NetDev]
Name=br0
Kind=bridge
EOF

cat <<EOF | sudo tee /etc/systemd/network/br0.network > /dev/null
[Match]
Name=br0

[Network]
Description=Virtual Bridge
DHCP=no
Address=10.10.1.1/24
IPForward=yes
EOF

cat <<EOF | sudo tee /etc/systemd/network/enp4s0f1.network > /dev/null
[Match]
Name=enp4s0f1

[Network]
Bridge=br0
EOF
