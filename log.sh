#!/usr/bin/env bash

# log the date and time in UTC
export TZ=UTC
# extract the Powder experiment id from the fully qualified hostname
EXPERIMENT=$(hostname | cut -d'.' -f2)
HOST=$(hostname | cut -d'.' -f1)
PROJECT=$GROUP
LOG_DIRECTORY=/proj/$PROJECT/logs/$EXPERIMENT
LOG_FILENAME=$LOG_DIRECTORY/$HOST.log
mkdir -p $LOG_DIRECTORY
while read line
do
        echo $line
        D=$(date +"%Y%m%d %H%M%S.%N")
        echo $D $line >> $LOG_FILENAME
done
