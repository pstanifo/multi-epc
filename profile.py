#!/usr/bin/env python
# 

"""Use this profile to create more than one EPC on a single node
using srsLTE release 20.04.1.

"""

# Library imports
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.rspec.pg as pg

class GLOBALS(object):
    SRSLTE_IMG = "urn:publicid:IDN+emulab.net+image+PowderProfiles:U18LL-SRSLTE:1"
    UBUNTU_1804_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"


def MakeRawPC(request, component_map, node_name, hardware_type):
    # Only use the hardware_type if the component_id is not specified.
    if component_map.has_key(node_name):
        # Component_id has been specified.
        node = request.RawPC(node_name, component_id=component_map[node_name])
        # remove the entry from the dictionary. If it's not empty at the end, generate an error.
        del component_map[node_name]
    else:
        node = request.RawPC(node_name)
        node.hardware_type = hardware_type

    return node

# Get Context and Request objects.
pc = portal.Context()
request = pc.makeRequestRSpec()

# Define profile parameters
portal.context.defineParameter(
    "startSrsLte",
    "Automatically start srsLTE software.",
    portal.ParameterType.BOOLEAN, True,
    [True, False],
    "If True, the scripts to start srsLTE will be run on the eNodeB, EPC and the UE. The IP routes are configured between the nodes.",
    advanced=True
)

portal.context.defineParameter(
    "nodenodetype",
    "additional node type",
    portal.ParameterType.STRING, "d430",
    ["","d710","d430","d740"],
    "Type of additional node attached to the EPC",
    advanced=True
)

portal.context.defineParameter(
    "epcnodetype",
    "EPC node type",
    portal.ParameterType.STRING, "d430",
    ["","d710","d430","d740"],
    "Type of EPC node",
    advanced=True
)

portal.context.defineParameter(
    "explicitcomponents",
    "Optional Explicit Component Ids",
    portal.ParameterType.STRING, "",
    longDescription="Optionally specify node component ids explicitly. In the form: nodename=componentname;nodename2=componentsname2. Example: enb1=nuc1;rue1=nuc3. If not specified, Powder assigns components based on the node type and the reservations",
    advanced=True
)


# Bind parameters
params = portal.context.bindParameters()

component_map = {}
if len(params.explicitcomponents) > 0:
    for kv in params.explicitcomponents.split(";"):
      kvlist = kv.split('=')
      if len(kvlist) != 2:
        portal.context.reportError(portal.ParameterError( "Explicit Components value is invalid %s" % kv,
            ['explicitcomponents'] ),
                immediate=True )
      component_map[kvlist[0].strip()] = kvlist[1].strip()


# Add EPC server, core network side
epc = MakeRawPC(request, component_map, "epc",  params.epcnodetype)
epc.disk_image = GLOBALS.SRSLTE_IMG
epc.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
epc.addService(rspec.Execute(shell="bash", command="/local/repository/add-nat-and-ip-forwarding.sh"))
if params.startSrsLte:
  epc.addService(rspec.Execute(shell="bash", command="/local/repository/start-impl.sh"))
  epc.addService(rspec.Execute(shell="bash", command="/local/repository/routes.sh"))

node = MakeRawPC(request, component_map, "node",  params.nodenodetype)
node.disk_image = GLOBALS.UBUNTU_1804_IMG
node_link = request.Link("node-link", members=[node, epc])

# if the map is not empty, at least one of the mappings was not used. In this case, generate an error.
if len(component_map) > 0:
    for key in component_map:
       portal.context.reportError(portal.ParameterError( "Explicit Component Id specified for unknown node %s" % key,
           ['explicitcomponents'] ),
               immediate=True )

pc.printRequestRSpec(request)
