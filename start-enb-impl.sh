#!/bin/bash

MYPATH=$(realpath $0 | xargs dirname)
if [[ $(getent hosts epc) == ""  ]]; then
  echo Start both eNodeB and EPC | $MYPATH/log.sh
  # combined eNode and EPC (using default parameters)
  # start srsepc
  sudo cp /local/repository/user_db.csv /etc/srslte/
  nohup sudo srsepc /etc/srslte/epc.conf 2>&1 > /tmp/epc.out.log&
  
  # start srsenb
  nohup sudo srsenb /etc/srslte/enb.conf 2>&1 > /tmp/enb.out.log&
else
  echo "Start eNodeB only with remote EPC " | $MYPATH/log.sh
  nohup /local/repository/start-enb-sep-impl.sh 2>&1 > /tmp/enb-out.log&
fi
