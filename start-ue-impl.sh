#!/bin/bash

MYPATH=$(realpath $0 | xargs dirname)
NODE_ID=$(geni-get client_id)
NUM=${NODE_ID:3}
HEXID=$(printf "%02x" $NUM)
echo "sudo srsue /etc/srslte/ue.conf --rf.dl_earfcn=2800,3400 --usim.imsi=0010101234567$HEXID" | $MYPATH/log.sh
sudo srsue /etc/srslte/ue.conf --rf.dl_earfcn=2800,3400 --usim.imsi=0010101234567$HEXID
