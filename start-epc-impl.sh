#!/bin/bash

MYPATH=$(realpath $0 | xargs dirname)
# bind to the interface towards the enb node(s)
EPC_ENB_IPADDR=$(getent hosts epc-link-1 | cut -s -d ' ' -f 1)
sudo cp /local/repository/user_db.csv /etc/srslte/
echo sudo srsepc /etc/srslte/epc.conf --mme.mme_bind_addr $EPC_ENB_IPADDR --spgw.gtpu_bind_addr $EPC_ENB_IPADDR | $MYPATH/log.sh
sudo srsepc /etc/srslte/epc.conf --mme.mme_bind_addr $EPC_ENB_IPADDR --spgw.gtpu_bind_addr $EPC_ENB_IPADDR
