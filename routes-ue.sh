#!/usr/bin/env bash

# To be run on the `rue1` compute node in the "scenario 2" profile.
# This sets up masquerading of IP traffic from downstream (`nodea`),
# and adds a route pushing most 10.0.0.0/8 traffic to the RAN uplink.

MYPATH=$(realpath $0 | xargs dirname)
UE_RAN_DEV="tun_srsue"
RTR_IP="172.16.0.1"
NDN_PORT="6363"
NODE_ID=$(geni-get client_id)
SNODE_ID="sue${NODE_ID: -1}"
SNODE_IPADDR=$(getent hosts $SNODE_ID | cut -s -d ' ' -f 1)
ERR=0

ip route add 10.0.0.0/8 via $RTR_IP || ERR=1
iptables -t nat -F || ERR=1
iptables -t nat -A POSTROUTING -o $UE_RAN_DEV -j MASQUERADE || ERR=1
iptables -t nat -A PREROUTING -i $UE_RAN_DEV -p tcp --dport $NDN_PORT -j DNAT --to-destination $SNODE_IPADDR || ERR=1
iptables -t nat -A PREROUTING -i $UE_RAN_DEV -p udp --dport $NDN_PORT -j DNAT --to-destination $SNODE_IPADDR || ERR=1

if [[ $ERR == 1 ]]; then
	echo $(basename $0) "Failed to add routes!"
else
	echo $(basename $0) "Finished adding routes."
fi | $MYPATH/log.sh

exit $ERR
