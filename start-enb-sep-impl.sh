#!/bin/bash

MYPATH=$(realpath $0 | xargs dirname)
# IP address of the epc node interface towards the enb node(s)
EPC_ENB_IPADDR=$(getent hosts epc-link-1 | cut -s -d ' ' -f 1)

# IP address of the interface towards the epc node
HOSTNAME=$(hostname | cut -s -d '.' -f 1)
ENB_EPC_IPADDR=$(getent hosts $HOSTNAME-link-1 | cut -s -d ' ' -f 1)
NUM=${HOSTNAME:3}
if [ $NUM == 1 ]; then
	# Set the DL EARFCN which specifies the DL frequency. The matching UL frequency is automatically selected.
	DL_EARFCN=3400
	ENB_ID=0x19B
else
	DL_EARFCN=2800
	ENB_ID=0x19C
fi

echo  sudo srsenb /etc/srslte/enb.conf --enb.enb_id=$ENB_ID --rf.dl_earfcn $DL_EARFCN --enb.mme_addr $EPC_ENB_IPADDR --enb.gtp_bind_addr $ENB_EPC_IPADDR --enb.s1c_bind_addr $ENB_EPC_IPADDR | $MYPATH/log.sh
sudo srsenb /etc/srslte/enb.conf --enb.enb_id=$ENB_ID --rf.dl_earfcn $DL_EARFCN --enb.mme_addr $EPC_ENB_IPADDR --enb.gtp_bind_addr $ENB_EPC_IPADDR --enb.s1c_bind_addr $ENB_EPC_IPADDR

