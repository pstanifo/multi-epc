#!/usr/bin/env bash

# Run appropriate setup script

NODE_ID=$(geni-get client_id)

if [[ $NODE_ID == "rue"* ]]; then
    nohup /local/repository/start-ue-impl.sh 2>&1 > /tmp/ue-out.log&
elif [[ $NODE_ID == "enb"* ]]; then
    /local/repository/start-enb-impl.sh 
elif [[ $NODE_ID == "epc" ]]; then
    nohup /local/repository/start-epc-impl.sh 2>&1 > /tmp/epc-out.log&
else
    echo "no setup necessary"
fi
